package com.ddzsoft.photogallery;

import android.support.v4.app.Fragment;

import com.ddzsoft.photogallery.fragment.PhotoGalleryFragment;


public class PhotoGalleryActivity extends SingleFragmentActivity {


    @Override
    protected Fragment createFragment() {
        return new PhotoGalleryFragment();
    }
}
