package com.ddzsoft.photogallery;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Frédéric on 04/08/2014.
 */
public class ThumbnailDownloader<Token> extends HandlerThread {

    private static final String TAG = "ThumbnailDownloader";
    private static final int MESSAGE_DOWNLOAD = 0;

    Handler mHandler;
    Map<Token, String> requestMap = Collections.synchronizedMap(new HashMap<Token, String>());

//    comes from the Main Thread
    Handler mResponseHandler;
    Listener<Token> mListener;

    public interface Listener<Token> {
        void onThumbnailDownloaded(Token token, Bitmap thumbnail);
    }

    public void setListener(Listener<Token> listener) {
        mListener = listener;
    }

    public ThumbnailDownloader(Handler responseHandler) {
        super(TAG);
        mResponseHandler = responseHandler;
    }

    public void queueThumbnail(Token token, String url) {
        Log.i(TAG, "Got an URL: " + url);

        requestMap.put(token, url);

        mHandler.obtainMessage(MESSAGE_DOWNLOAD, token)
                .sendToTarget();
    }

//    Called before the looper checks the queue for the first time
//    That's why the Handler is implemented here
    @Override
    protected void onLooperPrepared() {
        mHandler = new Handler() {

//            We check the message type, retrieve the token and pass it to the handleRequest()
            @SuppressWarnings("unchecked")
            @Override
            public void handleMessage(Message msg) {
                if(msg.what == MESSAGE_DOWNLOAD){
                 Token token = (Token) msg.obj;
                    Log.i(TAG, "Got a request for url: " + requestMap.get(token));
                    handleRequest(token);
                }
            }

        };
    }

//    We get the image data here and then create a Bitmap
    private void handleRequest(final Token token) {

        try {
//            Check if the url exists
            final String url = requestMap.get(token);
            if(url != null){

                byte[] bitmapBytes = new FlickrFetchr().getUrlBytes(url);
                final Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
                Log.i(TAG, "Bitmap created");

/*                Causes the Runnable to be added to the message queue. The runnable will be run
                on the thread to which this handler is attached.*/
                mResponseHandler.post(new Runnable() {
                    @Override
                    public void run() {
//                        Check the requestMap
                        if(requestMap.get(token) == url){
                            requestMap.remove(token);
                            mListener.onThumbnailDownloaded(token, bitmap);
                        }
                    }
                });
            }
        } catch (IOException ioe) {
            Log.e(TAG, "Error downloading image", ioe);
        }

    }

    public void clearQueue() {
//        Remove any pending posts of messages with code 'what' that are in the message queue
        mHandler.removeMessages(MESSAGE_DOWNLOAD);
        requestMap.clear();
    }
}
