package com.ddzsoft.photogallery.fragment;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.ddzsoft.photogallery.FlickrFetchr;
import com.ddzsoft.photogallery.R;
import com.ddzsoft.photogallery.ThumbnailDownloader;
import com.ddzsoft.photogallery.model.GalleryItem;

import java.util.ArrayList;

/**
 * Created by Frédéric on 02/08/2014.
 */
public class PhotoGalleryFragment extends Fragment {

    private static final String TAG = "PhotoGalleryFragment";

    int mFetchedPages = 0;
    int mTotalPages = 1;

    GridView mGridView;
    ArrayList<GalleryItem> mItems;
    ThumbnailDownloader<ImageView> mThumbnailThread;

    int mCurrentScrollState;
    int mCurrentFisrtVisibleItemIndex;
    int mCurrentVisibleItemCount;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
//        will start the AsyncTask and then fire up the background thread and call doInBackground
        new FetchItemsTask().execute();

        mThumbnailThread = new ThumbnailDownloader<ImageView>(new Handler());
        mThumbnailThread.setListener(new ThumbnailDownloader.Listener<ImageView>() {
            @Override
            public void onThumbnailDownloaded(ImageView imageView, Bitmap thumbnail) {
                if(isVisible()) {
                    imageView.setImageBitmap(thumbnail);
                }
            }
        });
        mThumbnailThread.start();
        mThumbnailThread.getLooper();
        Log.i(TAG, "Background thread started");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mThumbnailThread.quit();
        Log.i(TAG, "Background thread destroyed");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mThumbnailThread.clearQueue();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_photo_gallery, container, false);

        mGridView = (GridView) v.findViewById(R.id.gridView);

        return v;
    }

    /* AsyncTask constructor take in parameters:
      *  Void = Params
       * Void = Progress
       * ArrayList<GalleryItem> = Type of result */
    private class FetchItemsTask extends AsyncTask<Void, Void, ArrayList<GalleryItem>> {

        private FlickrFetchr mFlickrFetchr;

        @Override
        protected ArrayList<GalleryItem> doInBackground(Void... params) {

            mFlickrFetchr = new FlickrFetchr();
            return mFlickrFetchr.fetchItems();

        }

/*       Run after doInBackground() completes. Howewer, it's run of the MAIN THREAD,
        not the background thread, so it's safe to updated the UI within it*/
        @Override
        protected void onPostExecute(ArrayList<GalleryItem> items) {
            mItems = items;
            mFetchedPages++;
            mTotalPages = mFlickrFetchr.getTotalPages();

            setupAdapter();
        }

    }

    private class FetchNextPageTask extends AsyncTask<Void, Void, ArrayList<GalleryItem>> {

        @Override
        protected ArrayList<GalleryItem> doInBackground(Void... voids) {
            return new FlickrFetchr().fetchItems(mFetchedPages + 1, mItems);
        }

        @Override
        protected void onPostExecute(ArrayList<GalleryItem> items) {
            mItems = items;
            mFetchedPages++;
            ((GalleryItemAdapter) mGridView.getAdapter()).notifyDataSetChanged();

        }
    }

    private class GalleryItemAdapter extends ArrayAdapter<GalleryItem> {

        public GalleryItemAdapter(ArrayList<GalleryItem> items) {
            super(getActivity(), 0, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = getActivity().getLayoutInflater().inflate(R.layout.gallery_item, parent, false);
            }

            ImageView imageView = (ImageView) convertView.findViewById(R.id.gallery_item_imageView);
            imageView.setImageResource(R.drawable.brian_up_close);
            GalleryItem item = getItem(position);
            mThumbnailThread.queueThumbnail(imageView, item.getUrl());

            return convertView;
        }
    }

    public void setupAdapter() {
        if (getActivity() != null && mGridView != null) {
            if(mItems != null){

                mGridView.setAdapter(new GalleryItemAdapter(mItems));
                mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {

/*                    We need the parameters from both methods:

                        - onScrollStateChanged: scrollState
                            --> The state has to be "IDLE"! Previously the app was creating multiple FetchNextPageTask, as it was
                            called inside onScroll!

                        - onScroll:             firstVisibleItemIndex
                                                visibleItemCount
                                                (don't need the totalItemCount, we can get it from mItems)
                            --> To know if the last items are visible

                      So, the the easiest way was to created local variables that are updated in real time
                      And then, do the job in a new method!!...called isGridViewEndReached()


                        */
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                        mCurrentScrollState = scrollState;
                        isGridViewEndReached();
                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItemIndex, int visibleItemCount, int totalItemCount) {

                        mCurrentFisrtVisibleItemIndex = firstVisibleItemIndex;
                        mCurrentVisibleItemCount = visibleItemCount;

                    }
                });

            } else {
                mGridView.setAdapter(null);
            }
        }
    }

    private void isGridViewEndReached() {

//        If it's not scrolling anymore...
        if (mCurrentScrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
            Log.d(TAG, "SCROLL_STATE_IDLE");

//            ...and if the last items are visible
            if (mCurrentFisrtVisibleItemIndex + mCurrentVisibleItemCount == mItems.size()) {

                Log.d(TAG, "End of the GridView reached!");

                if (mFetchedPages < mTotalPages) {
                    Log.d(TAG, "Total pages:" + mTotalPages);

//                                We fetch the next page
                    new FetchNextPageTask().execute();

                }

            }
        }
    }

}
