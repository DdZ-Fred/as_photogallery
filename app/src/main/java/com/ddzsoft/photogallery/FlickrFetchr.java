package com.ddzsoft.photogallery;

import android.net.Uri;
import android.util.Log;

import com.ddzsoft.photogallery.model.GalleryItem;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Frédéric on 02/08/2014.
 */
public class FlickrFetchr {

    private static final String TAG = "FlickrFetchr";

    private static final String ENDPOINT = "https://api.flickr.com/services/rest/";
    private static final String API_KEY = "94479232f57e22380dab444313ee269e";
    private static final String PAGE = "page";
    private static final String METHOD_GET_RECENT = "flickr.photos.getRecent";
    private static final String PARAM_EXTRAS = "extras";
    private static final String EXTRA_SMALL_URL = "url_s";

    //    By default, only the page 1 is returned, containing 100 items "photo".
    private static final String XML_PHOTOS = "photos";
    private static final String XML_PHOTO = "photo";

    private int mTotalPages;


    public byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            /* HttpUrlConnection represents a connection, but it will not connect to the endpoint until I call
             * getInputStream(). Until then, i cannot get a valid response code! */
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }

            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }

    }

    public String getUrl(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }


//    Default fetchItems method, fetching the page 1 only!
    public ArrayList<GalleryItem> fetchItems() {

        ArrayList<GalleryItem> items = new ArrayList<GalleryItem>();

        try {
            /* buildUpon() returns a Uri.Builder...it allows us to build the complete URL for our
             * Flickr API Request. It's a convenience class for creating properly escaped parametized
              * URLs */
            String url = Uri.parse(ENDPOINT).buildUpon()
                    .appendQueryParameter("method", METHOD_GET_RECENT)
                    .appendQueryParameter("api_key", API_KEY)
                    .appendQueryParameter(PARAM_EXTRAS, EXTRA_SMALL_URL)
                    .build().toString();

            String xmlString = getUrl(url);
            Log.i(TAG, "Received xml: " + xmlString);
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(xmlString));


//            Page number initialization
            mTotalPages = getPages(parser);

//            Parse firstPage items
            parseItems(items, parser);


        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (XmlPullParserException xppe) {
            Log.e(TAG, "Failed to parse items", xppe);
        }


        return items;
    }

//    To fetch items from the defined page number
    public ArrayList<GalleryItem> fetchItems(int page, ArrayList<GalleryItem> items) {

        try {
            /* buildUpon() returns a Uri.Builder...it allows us to build the complete URL for our
             * Flickr API Request. It's a convenience class for creating properly escaped parametized
              * URLs */
            String url = Uri.parse(ENDPOINT).buildUpon()
                    .appendQueryParameter("method", METHOD_GET_RECENT)
                    .appendQueryParameter("api_key", API_KEY)
                    .appendQueryParameter(PARAM_EXTRAS, EXTRA_SMALL_URL)
                    .appendQueryParameter(PAGE, String.valueOf(page))
                    .build().toString();

            String xmlString = getUrl(url);
            Log.i(TAG, "Received xml: " + xmlString);
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(xmlString));

//                mTotalPages is initialized inside
            parseItems(items, parser);

        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (XmlPullParserException xppe) {
            Log.e(TAG, "Failed to parse items", xppe);
        }


        return items;
    }

    public void parseItems(ArrayList<GalleryItem> items, XmlPullParser parser) throws IOException, XmlPullParserException {

        int eventType = parser.next();

        while (eventType != XmlPullParser.END_DOCUMENT) {

            /* If type of event triggered is a Start Tag and that, the tag name is a "photo" then...*/
            if (eventType == XmlPullParser.START_TAG &&
                    XML_PHOTO.equals(parser.getName())) {

//                we get all the attributes
                String id = parser.getAttributeValue(null, "id");
                String caption = parser.getAttributeValue(null, "title");
                String smallUrl = parser.getAttributeValue(null, EXTRA_SMALL_URL);

//                ...create our item
                GalleryItem item = new GalleryItem();
                item.setId(id);
                item.setCaption(caption);
                item.setUrl(smallUrl);

//                ...and add it to the list
                items.add(item);
            }

//            we ask the parser to go the next event/occurrence
            eventType = parser.next();

        }

    }

    public int getPages(XmlPullParser parser) throws IOException, XmlPullParserException {

        int eventType = parser.next();
        int intPages = 0;

        while (eventType != XmlPullParser.END_DOCUMENT) {

            if (eventType == XmlPullParser.START_TAG &&
                    XML_PHOTOS.equals(parser.getName())) {

                String pages = parser.getAttributeValue(null, "pages");
                intPages = Integer.valueOf(pages);
                break;
            } else {
                eventType = parser.next();
            }

        }

        return intPages;

    }

    public int getTotalPages() {
        return mTotalPages;
    }

    public void setTotalPages(int totalPages) {
        mTotalPages = totalPages;
    }
}
